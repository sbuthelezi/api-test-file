# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

 You need Postman to execute the API tests.
 Pull the .JSON file to your local directory
 Open Postman and import the file by either dragging the file or navigate to the folder where the file is saved
 Once the file has been imported succesfully to Postman,you can run the collection.



 ### How to run tests ###
 
 Import the collection to your Postman application
 Click Run button on the top right corner.

### API tests ###

 The API tests are in the Google Books Collection.Google Books Collection has 2 GET requests.Both requests are authorized by API Key that has been provided on the requests.

1.GET Book Info
 -This requests returns a list of books that match the searchword and ISBN number provided.

2.GET Book Info_Author
 This requests returns a list of books that match the searchword and author name provided.

 ### Tests included in Google Books Collection ###

 1. Status code is 200
This tests that the status of the request is 200 (succesful)

2. Content-Type is present
 This tests that the content-Type is returned and that the content-type is JSON.
 
3. Body Matches string
This tests that the returned JSON results includes the provided string.

4. Get Book Authors
 This tests gets the Author name from the JSON results.Saves the Author name and uses it on GET Book Info_Author request.
 

### Who do I talk to? ###

* Repo Owner is Sihle Buthelezi
